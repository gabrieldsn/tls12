﻿using System;
using System.IO;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Windows.Forms;
using ws = TestTsl.FazendaService;

namespace TestTsl
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void TestarTls(string tlsVersao, bool isFazenda)
        {
            string ulrTeste = isFazenda ? "https://www1.nfe.fazenda.gov.br/NFeDistribuicaoDFe/NFeDistribuicaoDFe.asmx" : "https://api-aa-3t.sandbox.paypal.com";
            Uri myUri = new Uri(ulrTeste);

            using (var wc = new WebClient())
            {
                try
                {
                    GerarLog(Environment.NewLine + "Iniciando teste tls "+tlsVersao+" | "+myUri.Host);
                    ServicePointManager.SecurityProtocol = tlsVersao == "1.1" ? (SecurityProtocolType)768 : ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
                    
                    var content = wc.DownloadString(ulrTeste);
                   GerarLog("Conexão Tls "+tlsVersao+"  realizada com sucesso");
                }
                catch (Exception ex)
                {
                    if (isFazenda)
                    {
                        var mensagem = ex.Message.Contains("403")
                            ? "Conexão Tls " + tlsVersao + "  realizada com sucesso!\n"
                            : "Falha no teste. Erro: " + ex.Message;
                        GerarLog(mensagem);
                    }
                    else
                    {
                        var mensagem = ex.Message.Contains("404") ? "Conexão Tls " + tlsVersao + "  realizada com sucesso!\n"
                        : "Falha no teste. Erro: " + ex.Message;
                        GerarLog(mensagem);
                    }
                    
                }

            }
            GerarLog("--------------------------------");
        }

        private void GerarLog(string mensagem)
        {
            textBox1.AppendText(mensagem+Environment.NewLine);
        }

        private void btnTls1_Click(object sender, EventArgs e)
        {
            TestarTls("1.1", false);
        }

        private void btnTls2_Click(object sender, EventArgs e)
        {
            TestarTls("1.2", false);
        }

        private void btnFazenda_Click(object sender, EventArgs e)
        {
            TestarTls("1.1", true);
        }

        private void btnFazenda12_Click(object sender, EventArgs e)
        {
            TestarTls("1.2", true);
        }

        private void TestarTlsComCertificado()
        {
            try
            {
                GerarLog(Environment.NewLine + "Iniciando teste tls 1.2 com certificado | www1.nfe.fazenda.gov.br");
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | (SecurityProtocolType)768 | (SecurityProtocolType)3072 | SecurityProtocolType.Ssl3;
                X509Certificate2 meuCertificado = new X509Certificate2();
                X509Store store = new X509Store(StoreName.My, StoreLocation.CurrentUser);

                store.Open(OpenFlags.ReadOnly);

                foreach (X509Certificate2 certificate in store.Certificates)
                {
                    if (certificate.SerialNumber == "7E0E18020739F524")
                    {
                        meuCertificado = certificate;
                        break;
                    }
                }
                string html = string.Empty;
                string url = @"https://www1.nfe.fazenda.gov.br/NFeDistribuicaoDFe/NFeDistribuicaoDFe.asmx";

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.ClientCertificates.Add(meuCertificado);
                request.AutomaticDecompression = DecompressionMethods.GZip;
                request.KeepAlive = false;
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    html = reader.ReadToEnd();
                }

                Console.WriteLine(html);
                GerarLog("Conexão Tls 1.2 realizada com sucesso");
            }
            catch (Exception ex)
            {
                GerarLog("Falha no teste. Erro: " + ex.Message);
            }
        }
    }
}
