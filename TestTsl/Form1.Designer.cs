﻿namespace TestTsl
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnTls1 = new System.Windows.Forms.Button();
            this.btnTls2 = new System.Windows.Forms.Button();
            this.btnFazenda = new System.Windows.Forms.Button();
            this.btnFazenda12 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(27, 49);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(382, 206);
            this.textBox1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Log";
            // 
            // btnTls1
            // 
            this.btnTls1.Location = new System.Drawing.Point(123, 23);
            this.btnTls1.Name = "btnTls1";
            this.btnTls1.Size = new System.Drawing.Size(56, 23);
            this.btnTls1.TabIndex = 2;
            this.btnTls1.Text = "TLS 1.1";
            this.btnTls1.UseVisualStyleBackColor = true;
            this.btnTls1.Click += new System.EventHandler(this.btnTls1_Click);
            // 
            // btnTls2
            // 
            this.btnTls2.Location = new System.Drawing.Point(185, 23);
            this.btnTls2.Name = "btnTls2";
            this.btnTls2.Size = new System.Drawing.Size(56, 23);
            this.btnTls2.TabIndex = 2;
            this.btnTls2.Text = "TLS 1.2";
            this.btnTls2.UseVisualStyleBackColor = true;
            this.btnTls2.Click += new System.EventHandler(this.btnTls2_Click);
            // 
            // btnFazenda
            // 
            this.btnFazenda.Location = new System.Drawing.Point(247, 23);
            this.btnFazenda.Name = "btnFazenda";
            this.btnFazenda.Size = new System.Drawing.Size(78, 23);
            this.btnFazenda.TabIndex = 2;
            this.btnFazenda.Text = "Fazenda 1.1";
            this.btnFazenda.UseVisualStyleBackColor = true;
            this.btnFazenda.Click += new System.EventHandler(this.btnFazenda_Click);
            // 
            // btnFazenda12
            // 
            this.btnFazenda12.Location = new System.Drawing.Point(331, 23);
            this.btnFazenda12.Name = "btnFazenda12";
            this.btnFazenda12.Size = new System.Drawing.Size(78, 23);
            this.btnFazenda12.TabIndex = 2;
            this.btnFazenda12.Text = "Fazenda 1.2";
            this.btnFazenda12.UseVisualStyleBackColor = true;
            this.btnFazenda12.Click += new System.EventHandler(this.btnFazenda12_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(433, 267);
            this.Controls.Add(this.btnFazenda12);
            this.Controls.Add(this.btnFazenda);
            this.Controls.Add(this.btnTls2);
            this.Controls.Add(this.btnTls1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Name = "Form1";
            this.ShowIcon = false;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnTls1;
        private System.Windows.Forms.Button btnTls2;
        private System.Windows.Forms.Button btnFazenda;
        private System.Windows.Forms.Button btnFazenda12;
    }
}

